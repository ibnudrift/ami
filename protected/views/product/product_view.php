<div class="spacers_inside"></div>

<section class="product-sec-1 py-5">
    <div class="prelative container py-5">

        <div class="tops_content mb-4 pb-3 text-center">
            <h6 class="mb-0">Our Products</h6>
            <div class="py-1"></div>
            <h2><?php echo ucwords($strCategory->description->name) ?></h2>
            <div class="clear"></div>
        </div>

        <div class="middles_content">
            <?php if ($strCategory->image != ''): ?>
            <div class="poster-banner">
                <img src="<?php echo Yii::app()->baseUrl.'/images/category/'. $strCategory->image; ?>" alt="" class="img img-fluid">
            </div>
            <?php endif ?>

            <div class="py-4"></div>

            <div class="row">
                <div class="col-md-13">
                    
                    <div class="lefts_filter_content">
                        <h6>Browse products</h6>
                        <div class="py-2"></div>

                        <?php 
                        $criteria = new CDbCriteria;
                        $criteria->with = array('description');
                        $criteria->addCondition('t.parent_id = "0"');
                        // $criteria->params[':parents_id'] = $strCategory->parent_id;
                        $criteria->addCondition('t.type = :type');
                        $criteria->params[':type'] = 'category';
                        $criteria->order = 'sort ASC';
                        $CategorySubm = PrdCategory::model()->findAll($criteria);
                        ?>
                        <div class="lefts_mnu">
                            <ul class="list-unstyled">
                                <?php foreach ($CategorySubm as $key => $value): ?>
                                <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=> $value->id, 'slug'=> Slug::Create($value->description->name) )); ?>"><?php echo $value->description->name ?></a>
                                    <?php 
                                    $criteria = new CDbCriteria;
                                    $criteria->with = array('description');
                                    $criteria->addCondition('t.parent_id = :parents_id');
                                    $criteria->params[':parents_id'] = $value->id;
                                    $criteria->addCondition('t.type = :type');
                                    $criteria->params[':type'] = 'category';
                                    $criteria->order = 'sort ASC';
                                    $Cats_subt = PrdCategory::model()->findAll($criteria);
                                    ?>
                                    <?php if ($Cats_subt): ?>
                                    <ul>
                                        <?php foreach ($Cats_subt as $ke2 => $val2): ?>
                                        <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=> $val2->id, 'slug'=> Slug::Create($val2->description->name) )); ?>"><?php echo $val2->description->name; ?></a></li>
                                        <?php endforeach ?>
                                    </ul>
                                    <?php endif ?>
                                </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                        <div class="clear"></div>
                    </div>

                </div>
                <div class="col-md-47">
                    <div class="py-3 d-block d-sm-none"></div>
                    
                    <p class="bloc_sortby"><b>Sort by</b> &nbsp; &nbsp;<a href="#">Latest</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">$$$</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">$</a></p>

                    <div class="py-2"></div>
                    <!-- Start list products -->
                    <?php if (!empty($product)): ?>
                    <div class="lists_products_set_default">
                        <div class="row">
                            <?php foreach ($product->getData() as $key => $value): ?>
                            <div class="col-md-15 col-sm-30 col-30">
                                <div class="items">
                                    <div class="pict">
                                        <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=> $value->id, 'name'=>Slug::Create($value->description->name), 'category'=> $value->category_id )); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(264,264, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid"></a>
                                        </div>
                                    <div class="info">
                                        <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=> $value->id, 'name'=>Slug::Create($value->description->name), 'category'=> $value->category_id )); ?>"><h4><?php echo $value->description->name ?></h4></a>
                                        <p>2020 All season Collections</p>
                                        <?php if ($value->harga): ?>
                                        <div class="bc_prices">From IDR <?php echo $value->harga ?> / pcs</div>
                                        <?php endif ?>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <?php endif ?>
                    <!-- End list products -->

                </div>
            </div>


            <div class="clear"></div>
        </div>

    </div>
</section>