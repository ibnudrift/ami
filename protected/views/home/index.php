<section class="home-section-1 bg-white py-5">
    <div class="prelatife container">
        <div class="inner py-5 homes-text text-center">
            
            <h2 class="title-def">What We Create</h2>
            <div class="py-4 my-1"></div>

            <?php 
            $criteria = new CDbCriteria;
            $criteria->with = array('description');
            $criteria->addCondition('t.parent_id = "0"');
            $criteria->addCondition('t.type = :type');
            $criteria->params[':type'] = 'category';
            $criteria->limit = 3;
            $criteria->order = 't.sort ASC';
            $strCategory_all = PrdCategory::model()->findAll($criteria);
            ?>
            <div class="lists-def-banners-product">
                <div class="row">
                    <?php foreach ($strCategory_all as $key => $value): ?>
                    <div class="col-md-20">
                        <div class="items prelatife">
                            <div class="picture">
                                <img src="<?php echo $this->assetBaseurl.'../../images/category/'. $value->image2 ?>" alt="" class="img img-fluid">
                            </div>
                            <div class="info">
                                <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=> $value->id, 'slug'=> Slug::Create($value->description->name) )); ?>"><h4><?php echo $value->description->name ?></h4></a>
                                <p><?php echo $value->description->description ?></p>
                                <div class="py-2"></div>
                                <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=> $value->id, 'slug'=> Slug::Create($value->description->name) )); ?>" class="btn btn-link btns_bdefaults">View Collections</a>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>                    
                </div>
            </div>
            <div class="py-2"></div>

            <div class="clear clearfix"></div>
        </div>
    </div>
</section>

<section class="home-section-2 bg-trans">
    <div class="row">
        <div class="col-md-60">
            <div class="full-banner-def">
                <img src="<?php echo $this->assetBaseurl.'homes_bottom_pn1.jpg'; ?>" alt="" class="img img-fluid">
                <div class="desc_info text-center">
                    <div class="in_table">
                        <h3>Our Company</h3>
                        <p>Ami Collection, the name that invites you to grow and collaborate with limitless expansions.</p>
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>" class="btn btn-link btns_bdefaults">Learn More</a>
                    </div>
                </div>
            </div>        
        </div>
    </div>
    <div class="clear clearfix"></div>
    <div class="row no-gutters">
        <div class="col-md-30">
            <div class="full-banner-def tx2">
                <img src="<?php echo $this->assetBaseurl.'homes_bottom_pn_b1.jpg'; ?>" alt="" class="img img-fluid">
                <div class="desc_info text-left">
                    <div class="in_table">
                        <h3>Our Quality</h3>
                        <p>Learn about our strength, that’s quality</p>
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/quality')); ?>" class="btn btn-link btns_bdefaults">Learn More</a>
                    </div>
                </div>
            </div>   
        </div>
        <div class="col-md-30">
            <div class="full-banner-def tx2">
                <img src="<?php echo $this->assetBaseurl.'homes_bottom_pn_b2.jpg'; ?>" alt="" class="img img-fluid">
                <div class="desc_info text-left">
                    <div class="in_table">
                        <h3>Partnership</h3>
                        <p>Find the path towards success, together</p>
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/partner')); ?>" class="btn btn-link btns_bdefaults">Learn More</a>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</section>

<section class="home-section-3 bg-blues py-5">
    <div class="py-3"></div>
    <div class="prelatife container">
        <div class="inner homes-text text-center">
            <div class="tops_titl">
                <div class="row">
                    <div class="col-md-30">
                        <h2 class="title-def color-white">Our News & Articles</h2>
                    </div>
                    <div class="col-md-30">
                        <div class="text-right">
                            <a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>" class="btn btn-link views_n_allblog">View All</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="py-3"></div>

            <div class="clear"></div>
        </div>
    </div>

    <!-- Start list blog -->
    <div class="outers_blog_thems">
        <div class="nonloop owl-carousel">
            <?php for ($i=1; $i < 7; $i++) { ?>
            <div class="item">
              <div class="items_blog">
                  <div class="row no-gutters">
                      <div class="col-md-35">
                          <div class="pict">
                            <img src="<?php echo $this->assetBaseurl; ?>pix-ex-news.jpg" alt="" class="img img-fluid">
                            </div>
                      </div>
                      <div class="col-md-25 bg-white text-left">
                          <div class="p-5 desc">
                               <h3>News</h3>
                               <div class="py-2"></div>
                               <p>Ami Collection membuka store baby outlet yang ke 3 di Jawa Timur. Kota Malang dipercaya menjadi destinasi yang menjanjikan untuk penjualan retail maupun grosir pakaian anak koleksi new born dan baby.</p>
                               <div class="py-3"></div>
                               <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail')); ?>" class="btn btn-link btns_bdefaults">Read</a>
                              <div class="clear"></div>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <!-- End list blog -->

    <div class="py-3"></div>
</section>

<section class="home-section-4 bg-white py-5">
    <div class="prelatife container">
        <div class="inner py-4 homes-text text-center">
            <h2 class="titlt-def">Our Online Chanel</h2>
            <div class="py-2"></div>
            <ul class="list-inline listn_data_channel">
                <li class="list-inline-item"><a href="#"><img src="<?php echo $this->assetBaseurl.'icon-lg-lazada.png'; ?>" alt="" class="img img-fluid"> <br>Ami Collection</a></li>
                <li class="list-inline-item"><a href="#"><img src="<?php echo $this->assetBaseurl.'icon-lg-instagram.png'; ?>" alt="" class="img img-fluid"> <br>Amicolection.id</a></li>
                <li class="list-inline-item"><a href="#"><img src="<?php echo $this->assetBaseurl.'icon-lg-instagram.png'; ?>" alt="" class="img img-fluid"> <br>Amihealth.id</a></li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script type="text/javascript">
    $(function(){
        $('.nonloop').owlCarousel({
            center: true,
            items:2,
            loop:false,
            margin:10,
            autoplay:true,
            responsive:{
                600:{
                    items:2
                }
            }
        });
    });
</script>