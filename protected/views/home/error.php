<div class="blocks_subpage_banner tops_herror">
  <div class="container prelatife text-center">

    <div class="py-5"></div>

    <h3 class="sub_title_p">Error <?php echo $error['code'] ?></h3>
    <div class="clear"></div>
    <h5><?php echo $error['message'] ?></h5>
    <div class="py-5"></div>

    <div class="clear"></div>
  </div>
</div>

<style type="text/css">
    .tops_herror{
        border-top: 3px solid #ccc;
    }
    .tops_herror h3{
        font-size: 35px;
        margin-bottom: 15px;
    }
</style>