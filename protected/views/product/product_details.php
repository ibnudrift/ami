<div class="spacers_inside"></div>

<section class="product-sec-1 py-5">
    <div class="prelative container py-5">

        <div class="tops_content mb-4 pb-3 text-center">
            <h2><?php echo $category->description->name; ?></h2>
            <div class="clear"></div>
        </div>

        <div class="middles_content boxeds_product_detail">
            <div class="py-2"></div>
            <div class="row">
                <div class="col-md-30">
                    <div class="blocks_left_pictprd">
                        <div class="picture pictures_bg">
                            <a class="fancybox" href="<?php echo $this->assetBaseurl. '../../images/product/'. $data->image; ?>"><img src="<?php echo $this->assetBaseurl. '../../images/product/'. $data->image; ?>" alt="" class="img img-fluid"></a>
                        </div>
                        <div class="pct_thumbs">
                            <ul class="list-inline lists_other_nitem">
                                <li class="list-inline-item" data-src="<?php echo $this->assetBaseurl. '../../images/product/'. $data->image; ?>">
                                    <img src="<?php echo $this->assetBaseurl. '../../images/product/'. $data->image; ?>" alt="" class="img img-fluid">
                                </li>
                                <?php 
                                $drn_images = PrdProductImage::model()->findAll('product_id = :prd_id', array(':prd_id'=> $data->id));
                                ?>
                                <?php if (!empty($drn_images)): ?>
                                    <?php foreach ($drn_images as $key => $value): ?>
                                     <li class="list-inline-item" data-src="<?php echo $this->assetBaseurl. '../../images/product/'. $value->image; ?>">
                                        <img src="<?php echo $this->assetBaseurl. '../../images/product/'. $value->image; ?>" alt="" class="img img-fluid">
                                    </li>   
                                    <?php endforeach ?>
                                <?php endif ?>
                            </ul>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <script type="text/javascript">
                        $(function(){
                          var object_bg = $('.pictures_bg');
                          var lst_thumb_item = $('.lists_other_nitem li');

                          $(lst_thumb_item).on('mouseenter', function(){
                            var imgs_fn = $(this).children().attr('src');
                            $(object_bg).find('img').attr('src', imgs_fn);
                            $(object_bg).find('a').attr('href', imgs_fn);
                            return false;
                          });

                          $('.fancybox').fancybox();

                        });  
                      </script>
                </div>
                <div class="col-md-30">
                    <div class="d-block d-sm-none py-3"></div>
                    <div class="description">
                        <h4><?php echo ucwords($data->description->name); ?></h4>
                        <h6>2020 All season Collections</h6>
                        <div class="py-3"></div>
                        <div class="sub_section">
                            <h6 class="mb-1">Description</h6>
                            <?php echo $data->description->desc; ?>
                        </div>
                        <?php if (!empty($data->p1_qty) && !empty($data->p1_prices)): ?>
                            <div class="sub_section">
                                <h6 class="mb-1">Price</h6>
                                <div class="row">
                                    <div class="col-15">Min of <?php echo $data->p1_qty ?></div>
                                    <div class="col blues">IDR <?php echo ($data->p1_prices) ?> / PCS</div>
                                </div>
                                <div class="row">
                                    <div class="col-15">Min of <?php echo $data->p2_qty ?></div>
                                    <div class="col blues">IDR <?php echo ($data->p2_prices) ?> / PCS</div>
                                </div>
                                <div class="row">
                                    <div class="col-15">Min of <?php echo $data->p3_qty ?></div>
                                    <div class="col blues">IDR <?php echo ($data->p3_prices) ?> / PCS</div>
                                </div>
                                
                            </div>
                        <?php else: ?>
                            <div class="sub_section">
                                <h6 class="mb-1">Price</h6>
                                <div class="row">
                                    <div class="col blues">Call for Prices</div>
                                </div>
                            </div>
                        <?php endif ?>
                        <div class="py-3"></div>
                        <a href="#" class="btn btn-link btns_bdefaults"><img src="<?php echo $this->assetBaseurl ?>icn-wa.png" alt=""> &nbsp;Inquire Via Whatsapp Now</a>

                        <div class="py-3"></div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>

    </div>
</section>

<section class="product-sec-2 py-5">
    <div class="prelative container py-3">

        <div class="tops_other content-text">
            <div class="row">
                <div class="col-md-30">
                    <h5 class="m-0 mb-0">You may also like</h5>
                </div>
                <div class="col-md-30">
                    <div class="py-1"></div>
                    <div class="text-right backs_collect">
                        <a onclick="window.history.go(-1); return false;" href="#">Back to collection</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-3"></div>

        <?php if (!empty($product)): ?>
        <!-- Start list products -->
        <div class="lists_products_set_default">
            <div class="row">
                <?php foreach ($product as $key => $value): ?>
                    <div class="col-md-15 col-sm-30 col-30">
                        <div class="items">
                            <div class="pict">
                                <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=> $value->id, 'name'=>Slug::Create($value->description->name), 'category'=> $value->category_id )); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(264,264, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid"></a>
                                </div>
                            <div class="info">
                                <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=> $value->id, 'name'=>Slug::Create($value->description->name), 'category'=> $value->category_id )); ?>"><h4><?php echo $value->description->name ?></h4></a>
                                <p>2020 All season Collections</p>
                                <div class="bc_prices">From IDR 20,000 / pcs</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
            </div>
        </div>
        <!-- End list products -->
        <?php endif ?>

        <div class="clear"></div>
    </div>
</section>

<link rel="stylesheet" type="text/css" href="/asset/js/sweetalert/sweetalert.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
