<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl2 ?>hero-contacts.jpg');">
    <div class="prelative container py-5">
        <div class="py-5"></div>
        <div class="row py-5">
            <div class="col-md-30 py-5">
                <div class="insides_intext wow slideInLeft">
                    <h3 class="mb-2">KONTAK</h3>
                    <div class="py-1"></div>
                    <p>EKLIN Fertilizer Group. senantiasa siap melayani kebutuhan informasi yang anda perlukan.</p>
                    <div class="clear"></div>
                </div>

            </div>
            <div class="col-md-30"></div>
        </div>
    </div>
</section>

<section class="outers-contact-block-1 py-5">
  <div class="prelatife container">
    <div class="inners-default-content block-contact py-5 wow fadeInUp">
      <div class="sub-title">FORM INKUIRI EKLIN FERTILIZER GROUP</div>
      <div class="py-3"></div>
      <div class="row content-text">
        <div class="col-md-40">
          <h5 class="mb-2 pb-1">Untuk segala macam inkuiri tentant produk dan marketing, silahkan tinggalkan pesan.</h5>
          <p>Mohon menuliskan detil anda di bawah, staf sales kami akan segera menghubungi anda kembali.</p>
          <div class="py-2"></div>
          
          <div class="inners_form_contact">
            <div class="py-2"></div>
            <?php echo $this->renderPartial('//home/_form_contact2', array('model'=> $model)); ?>
          </div>
          <div class="clear clearfix"></div>
        </div>
        <div class="col-md-20"></div>
      </div>

      <div class="py-4"></div>
      <div class="lines-grey"></div>
      <div class="py-4 my-4"></div>
      <div class="sub-title mb-5">INFORMASI KONTAK EKLIN GROUP</div>
      <p class="calls_wa"><span>WHATSAPP ONLINE</span> <small>&nbsp;</small> <a href="https://wa.me/62811309289"><i class="fan_wa"></i> +62 81 130 9289</a> <small>&nbsp;&nbsp;</small> <a href="https://wa.me/62811309329"><i class="fan_wa"></i> +62 81 1309 329</a></p>
      <div class="py-3 my-1"></div>

      <div class="row content-text listncontacts_companyaddress">
        <?php 
        // $model = Addresscompany::model()->findAll("t.status = 1 ORDER BY t.id DESC")
        ?>
        <?php // foreach ($model as $key => $value): ?>
        <div class="col-md-20">
          <div class="stext">
            <h5 clss="mb-1"><b>Surabaya Head Office</b></h5>
              <p>Vila Bukit Mas RE-18<br>Jl. Abdul Wahab Siamin<br>Surabaya, Jawa Timur<br>Phone : +62 (31) 567 2600, 563 1568<br>Fax : +62 (31) 566 8619<br>Email : <a href="mailto:info@eklingroup.com">info@eklingroup.com</a></p>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-20">
          <div class="stext">
            <h5 clss="mb-1"><b>Gresik Fertilizer Factory</b></h5>
              <p><b>CV.BIR</b><br> Jl. Raya Daendels KM 46,8 No.89 <br> Kel. Golokan, Kec. Sidayu, Kab. Gresik, Prov. Jawa Timur</p>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-20">
          <div class="stext">
            <h5 clss="mb-1"><b>Krian Fertilizer Factory</b></h5>
              <p><b>PT.BIR</b><br> Jl. Raya By Pass Krian KM 1,2 <br> RT.014 RW. 006, Kel. Watugolong, Kec. Krian, Kab. Sidoarjo, Prov. Jawa Timur</p>
            <div class="clear"></div>
          </div>
        </div>
        
        <?php // endforeach ?>

      </div>

      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>
</section>

<style type="text/css">
  section.outers-contact-block-1 .inners-default-content.block-contact .content-text h5{
    line-height: 1.3;
  }
</style>