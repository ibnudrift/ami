<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl ?>hero-quality.jpg');">
    <div class="inner_table">
        <div class="prelative container py-5">
            <div class="row py-5">
                <div class="col-md-60 py-5">
                    <div class="insides_intext wow slideInDown text-center">
                        <h3 class="mb-2">Our Quality</h3>
                        <div class="py-1"></div>
                        <p>Quality is at the heart of everything we do</p>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="quality-sec-1 py-5">
    <div class="prelative container py-5">
        <div class="row text-center wow fadeInUp">
            <div class="col-md-60 text-center">
                <div class="content-text">
                    <h5>Every day, our creations either baby clothing products, uniforms, apparels, non-medical masks and many more, they will touch thousands of lives. It’s our responsibility to make sure that all product interacted with people are safe and performs as planned. By becoming a quality caring company, we go even further in making our customer a happier, healthier people. Together with AMI Collection as your garment & clothing convection factory, we can have the potential to increase customer loyalty and satisfaction while gaining profits.</h5>
                    <p>We have translated our passion for quality into four the “AMI Quality Compass”. The compas served as a guidance where every direction is important and bound to each other. This compass will be used to track our achievements towards quality and resulting on progress report on a regular basis. Customer can have a site visit to be the witness that we are living and working with this quality purpose every day.</p>

                    <div class="py-4"></div>
                    <h2 class="def-title">AMI Quality Compass</h2>
                    <div class="py-4"></div>
                    <div class="row">
                        <div class="col-md-15">
                            <div class="py-4"></div>
                            <div class="text-center">
                                <h5>Act</h5>
                                <p>Control chart, reporting, problem finding, alarm every defects.</p>
                            </div>
                            <div class="py-5"></div>
                            <div class="py-5"></div>
                            <div class="text-center">
                                <h5>Check</h5>
                                <p>Key Perfomance Index at all time, making sure SOP is performed in every way.</p>
                            </div>
                        </div>
                        <div class="col-md-30">
                            <div class="banner-pict"><img src="<?php echo $this->assetBaseurl; ?>compass-quality-banner.png" alt="" class="img img-fluid"></div>
                        </div>
                        <div class="col-md-15">
                            <div class="py-4"></div>
                            <div class="text-center">
                                <h5>Plan</h5>
                                <p>Target, limit, product development, measure and test.</p>
                            </div>
                            <div class="py-5"></div>
                            <div class="py-5"></div>
                            <div class="text-center">
                                <h5>Do</h5>
                                <p>Quality measurement and control, product sampling, conformity.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


<section class="quality-sec-2 py-5">
    <div class="prelative container py-5">
        <div class="py-3"></div>
        <div class="row text-center wow fadeInUp">
            <div class="col-md-5"></div>
            <div class="col-md-50 text-center content-text">
                <h2 class="def-title">Our Commitment</h2>
                <div class="py-2"></div>
                <h5>AMI Colection is your global partner that always be by your side.</h5>
                <p>With an emphasis on bringing innovation, creativity and knowledge to our customers, we will always be by your side, bringing insights and expertise to develop new style and introduce probabilities that delight consumers’ ever-shifting desires towards fashion garment.</p>
                <div class="py-3"></div>
                <div class="dnblocks">
                    <img src="<?php echo $this->assetBaseurl.'banner-quality-2.jpg' ?>" alt="" class="img img-fluid">
                </div>
                <div class="py-3 my-2"></div>
                <div class="maw40pers d-block mx-auto">
                    <p>“Our quality standard will be the compass to guide our development and set strategic goals. It’s a bold ambition to grow our business by putting zero tolerance towards defects, and because we are AMI Collection, the brand that indentical with quality: our hearts embrace it as it has become our pride.”</p>
                    <p class="names">Nelson Gosaria <br>
                    <i>Founder</i></p>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>
    </div>
</section>