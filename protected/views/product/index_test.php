<?php
error_reporting(1);

$dbcon = array(
			'host'=>'localhost',
			'user'=>'khayanga_root',
			'pass'=>'1q2w3e4r5t6y',
			'db'=>'khayanga_neww',
	);
// $dbcon = array(
// 			'host'=>'localhost',
// 			'user'=>'root',
// 			'pass'=>'1q2w3e4r5t6y',
// 			'db'=>'khayangan',
// );

require_once __DIR__.'/../vendor/autoload.php';
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

// captcha
use Gregwar\Captcha\CaptchaBuilder;

$app = new Silex\Application();

// conecting database 
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array (
        'mysql_read' => array(
            'driver'    => 'pdo_mysql',
            'host'      => $dbcon['host'],
            'dbname'    => $dbcon['db'],
            'user'      => $dbcon['user'],
            'password'  => $dbcon['pass'],
            'charset'   => 'utf8',
        ),
    ),
));

/* Global constants */
define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT']);
define('APP_PATH', dirname(ROOT_PATH).DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR);
define('ASSETS_PATH', ROOT_PATH.DIRECTORY_SEPARATOR);

// http cache
$app->register(new Silex\Provider\HttpCacheServiceProvider(), array(
    'http_cache.cache_dir' => __DIR__.'/cache/',
));

// Register Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

// Register Swiftmailer
$app->register(new Silex\Provider\SwiftmailerServiceProvider());

// Register URL Generator
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// Load JS

// '/assets/js/bootstrap.js',

$app['js'] = array(
	'/assets/js/jquery.min.js',
	// nivo
	'/assets/js/nivo-slider/jquery.nivo.slider.pack.js',
	'/assets/js/jquery.scrollTo-1.4.3.1-min.js',
	'/assets/js/jquery-migrate-1.2.1.min.js',
	// fancy
	'/assets/js/fancy/jquery.fancybox.pack.js',
	
	'/assets/vendors/superfish.min.js',
	'/assets/vendors/magnefic-popup/jquery.magnific-popup.min.js',
	'/assets/vendors/nice-select/jquery.nice-select.min.js',
	'/assets/vendors/js/main.js',
	'/assets/vendors/jquery.ajaxchimp.min.js',

	'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',
	'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js',
	'https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js',

	'/assets/vendors/magnefic-popup/jquery.magnific-popup.min.js',
	'/assets/vendors/superfish.min.js',
	'/assets/vendors/nice-select/jquery.nice-select.min.js',
	'/assets/vendors/jquery.ajaxchimp.min.js',
	'/assets/vendors/slick-1.8.1/slick/slick.min.js',
);





// Load CSS
$app['css'] = array(
	'/assets/css/screen.css',
	'/assets/css/comon.css',

	//cek kalau bootstrap.css di hapus link menu mau keluar
	
	'/assets/css/styles.css',

	'/assets/css/media.styles.css',

	'/assets/js/nivo-slider/nivo-slider.css',
	'/assets/js/nivo-slider/themes/default/default.css',

	'/assets/js/fancy/jquery.fancybox.css',

	'/assets/vendors/bootstrap/bootstrap.min.css',
	'/assets/vendors/fontawesome/css/all.min.css',
	'/assets/vendors/themify-icons/themify-icons.css',
	'/assets/vendors/linericon/style.css',
	'/assets/vendors/magnefic-popup/magnific-popup.css',
	'/assets/vendors/owl-carousel/owl.theme.default.min.css',
	'/assets/vendors/owl-carousel/owl.carousel.min.css',
	'/assets/vendors/nice-select/nice-select.css',
	'/assets/vendors/slick-1.8.1/slick/slick.css',
	'/assets/vendors/slick-1.8.1/slick/slick-theme.css',
	'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',

);

$titlePage = 'Perumahan Khayangan Residence Bangkalan - Properti Perumahan Madura';

$app->get('/', function () use ($app, $titlePage) {
	return $app['twig']->render('page/homepage.twig', array(
        'layout' => 'layouts/main.twig',
        'title'=>$titlePage,
    ));
})
->bind('homepage');

$app->get('/about', function () use ($app, $titlePage) {
	// $app['active'] = 'class="active"';
    return $app['twig']->render('page/about.twig', array(
        'layout' => 'layouts/main.twig',
        'active'=>'about',
        'title'=>$titlePage,
    ));
})->bind('about');

$app->get('/pilih_type', function () use ($app, $titlePage) {

    return $app['twig']->render('page/pilih_type.twig', array(
        'layout' => 'layouts/main.twig',
        'title'=>$titlePage,
    ));
})->bind('pilih_type');

$app->get('/ruko', function () use ($app, $titlePage) {

    return $app['twig']->render('page/ruko.twig', array(
        'layout' => 'layouts/main.twig',
        'title'=>$titlePage,
    ));
})->bind('ruko');

$app->get('/ruko_detail/{id}', function ($id) use ($app, $titlePage) {

	$data = array(
		1=>array(
			'nama'=>'RB Side Road',
			'images'=>'RB-Side-Road.jpg',
			'images2'=>'DSC07900.jpg',
			'lantai'=>'2 Lantai',
			'dimensi'=>'5x17',
			),
		2=>array(
			'nama'=>'RB Frontage Road',
			'images'=>'RB-Frontage-Road.JPG',
			'images2'=>'DSC07902.jpg',
			'lantai'=>'3 Lantai',
			'dimensi'=>'5x18',
			)
		);
	$id = abs((int) $id);
	if ($id != 1 && $id != 2) {
		return $app->redirect( $app['url_generator']->generate('homepage') );
	}

	$data = $data[$id];

    return $app['twig']->render('page/ruko_detail.twig', array(
        'layout' => 'layouts/main.twig',
        'title'=>$titlePage,
        'data'=>$data,
    ));
})->bind('ruko_detail');

$app->get('/type/{param}', function ($param) use ($app, $titlePage) {
	$data = $app['db']->fetchAll('SELECT * FROM type_bangunan order by sort asc');

	$cluster = $app['db']->fetchAll('SELECT cluster FROM type_bangunan Group By cluster order by sort asc');
	if($param == 'ALL'){
		$cluster1 = $app['db']->fetchAll('SELECT * FROM type_bangunan WHERE cluster = "GRAND ROAD" order by sort asc');
		$cluster2 = $app['db']->fetchAll('SELECT * FROM type_bangunan WHERE cluster = "MAHABARATA I" order by sort asc');
		$cluster3 = $app['db']->fetchAll('SELECT * FROM type_bangunan WHERE cluster = "MAHABARATA II" order by sort asc');
		$cluster4 = $app['db']->fetchAll('SELECT * FROM type_bangunan WHERE cluster = "NIRWANA" order by sort asc');
		$cluster5 = $app['db']->fetchAll('SELECT * FROM type_bangunan WHERE cluster = "ARJUNA" order by sort asc');

	}else if($param == 'GRAND ROAD'){
		$cluster1 = $app['db']->fetchAll('SELECT * FROM type_bangunan WHERE cluster = "GRAND ROAD" order by sort asc');
		$cluster2 = null;
		$cluster3 = null;
		$cluster4 = null;
		$cluster5 = null;
	}else if($param == 'MAHABARATA I'){
		$cluster1 = null;
		$cluster2 = $app['db']->fetchAll('SELECT * FROM type_bangunan WHERE cluster = "MAHABARATA I" order by sort asc');
		$cluster3 = null;
		$cluster4 = null;
		$cluster5 = null;
	}else if($param == 'MAHABARATA II'){
		$cluster1 = null;
		$cluster2 = null;
		$cluster3 = $app['db']->fetchAll('SELECT * FROM type_bangunan WHERE cluster = "MAHABARATA II" order by sort asc');
		$cluster4 = null;
		$cluster5 = null;
	}else if($param == 'NIRWANA'){
		$cluster1 = null;
		$cluster2 = null;
		$cluster3 = null;
		$cluster4 = $app['db']->fetchAll('SELECT * FROM type_bangunan WHERE cluster = "NIRWANA" order by sort asc');
		$cluster5 = null;
	}else if($param == 'ARJUNA'){
		$cluster1 = null;
		$cluster2 = null;
		$cluster3 = null;
		$cluster4 = null;
		$cluster5 = $app['db']->fetchAll('SELECT * FROM type_bangunan WHERE cluster = "ARJUNA" order by sort asc');
	}
	

    return $app['twig']->render('page/type.twig', array(
        'layout' => 'layouts/main.twig',
		'data' => $data,
		'cluster' => $cluster,
		'cluster1' => $cluster1,
		'cluster2' => $cluster2,
		'cluster3' => $cluster3,
		'cluster4' => $cluster4,
		'cluster5' => $cluster5,
        'title'=>$titlePage,
    ));
})->bind('type');


$app->get('/news', function () use ($app, $titlePage) {
	$data = $app['db']->fetchAll('SELECT * FROM news order by sort asc');
    return $app['twig']->render('page/news.twig', array(
        'layout' => 'layouts/main.twig',
        'data' => $data,
        'title'=>$titlePage,
    ));
})->bind('news');

$app->get('/news-detail/{id}', function ($id) use ($app, $titlePage) {
	$dataAll = $app['db']->fetchAll('SELECT * FROM news order by sort asc');

	$sql = "SELECT * FROM news WHERE id = ?";
	$post = $app['dbs']['mysql_read']->fetchAssoc($sql, array((int) $id));

    return $app['twig']->render('page/news-detail.twig', array(
        'layout' => 'layouts/main.twig',
         'data' => $post,
         'dataAll'=>$dataAll,
         'title'=>$titlePage,
         'interior'=>$interior,
    ));
})->bind('news-detail');


$app['interior'] = array(
				'berlian'=> array(
					'folder'=>'Unit Interior Berlian',
					'list'=>array(
						'001-living.jpg',
						'002-dining.jpg',
						'004-entrance.jpg',
						'005-credenza.jpg',
						'006-dine.jpg',
						'007-dine.jpg',
						'008-entrance.jpg',
						'009-master.jpg',
						'010-master.jpg',
						'011-master.jpg',
						'012-living.jpg',
						'013-bath.jpg', 
						),
					),
				'safir' => array(
					'folder'=>'Unit Interior Safir (9x15)',
					// 'prnt'=>'DSC01812 a.jpg',
					'list'=>array(
						'DSC01812 a.jpg',
						'IMG_7004 a.jpg',
						'IMG_7009 a.jpg',
						'IMG_7018 a.jpg',
						'IMG_7031 a.jpg',
						'IMG_7040 a.jpg',
						'IMG_7045 a.jpg',
						),
					),
				'mutiara'=> array(
					'folder'=>'Unit Interior Mutiara (6x15)',
					// 'prnt'=>'IMG_7309 a.jpg',
					'list'=>array(
						'IMG_7309 a.jpg',
						'IMG_7321 a.jpg',
						'IMG_7323 a.jpg',
						'IMG_7335 a.jpg',
						),
					),
				'ruby'=> array(
					'folder'=>'Unit Interior Ruby (8x15)',
					// 'prnt'=>'DSC01816 a.jpg',
					'list'=>array(
						'DSC01816 a.jpg',
						'IMG_7273 copy.jpg',
						'IMG_7278 copy.jpg',
						'IMG_7282 a.jpg',
						'IMG_7287 copy.jpg',
						'IMG_7298 copy.jpg',
						),
					),
			);

$app->get('/type-detail/{id}', function ($id) use ($app, $titlePage) {
	$dataAll = $app['db']->fetchAll('SELECT * FROM type_bangunan order by sort asc');

	$sql = "SELECT * FROM type_bangunan WHERE id = ?";
	$post = $app['dbs']['mysql_read']->fetchAssoc($sql, array((int) $id));

   // berlian
	$interior = array();
	if ($id == 17) {
		$interior = $app['interior']['berlian'];
	// safir
	}elseif ($id == 4) {
		$interior = $app['interior']['safir'];
	}elseif ($id == 5 || $id == 8 || $id == 9 || $id == 10 || $id == 11){
		$interior = $app['interior']['ruby'];
	}elseif ($id == 1 || $id == 6 || $id == 7){
		$interior = $app['interior']['mutiara'];
	} else {
		$interior = array();
	}

	// print_r($interior);
	// exit;

    return $app['twig']->render('page/type-detail.twig', array(
        'layout' => 'layouts/main.twig',
         'data' => $post,
         'dataAll'=>$dataAll,
         'title'=>$titlePage,
         'interior'=>$interior,
    ));
})->bind('type-detail');

$app->get('/map', function () use ($app, $titlePage) {
    return $app['twig']->render('page/map.twig', array(
        'layout' => 'layouts/main.twig',
        'title'=>$titlePage,
    ));
})->bind('map');

$app->get('/facility', function () use ($app, $titlePage) {
    return $app['twig']->render('page/facility.twig', array(
        'layout' => 'layouts/main.twig',
        'title'=>$titlePage,
    ));
})->bind('facility');

$app->get('/gallery', function () use ($app, $titlePage) {
    return $app['twig']->render('page/gallery.twig', array(
        'layout' => 'layouts/main.twig',
        'title'=>$titlePage,
    ));
})->bind('gallery');


$app->get('/invest', function () use ($app, $titlePage) {
    return $app['twig']->render('page/invest.twig', array(
        'layout' => 'layouts/main.twig',
        'title'=>$titlePage,
    ));
})->bind('invest');

$app->register(new Silex\Provider\ValidatorServiceProvider());

$app->get('/contact', function () use ($app, $titlePage) {
	$constraint = array(
	    'name' => '',
	    'email' => '',
	    'telp' => '',
		'message' => '',
	);

	// $builder = new CaptchaBuilder;
	// $builder->build();

    return $app['twig']->render('page/contact.twig', array(
        'layout' => 'layouts/main.twig',
        'errors' => array(),
        'data' => $constraint,
        'title'=>$titlePage,
    ));
})->bind('contact');


$app->post('/contact', function (Request $request) use ($app, $titlePage) {
    $message = $request->get('data');
    $email = $request->get('email');

	$constraint = new Assert\Collection(array(
	    'name' => new Assert\NotBlank(),
	    'email' => array(new Assert\Email(), new Assert\NotBlank()),
		'telp' => new Assert\NotBlank(),
		'message' => new Assert\NotBlank(),
	));
	
	$errors = $app['validator']->validateValue($data, $constraint);
	$errorMessage = array();
	// echo $_POST['g-recaptcha-response']; exit; 

	if (!isset($_POST['g-recaptcha-response'])) {
		$errorMessage[] = 'Please Check Captcha for sending contact form!';
	}

	$secret_key = "6LfxZSwUAAAAACk0rs2DXlwJOotXU9YZFCDO1_Tq";
	$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);

	$response = json_decode($response);
	if($response->success==false)
	{
		$errorMessage[] = 'Please Check Captcha for sending contact form!';
	}
	if (count($errors) > 0) {
		return $app['twig']->render('page/contact.twig', array(
	        'layout' => 'layouts/main.twig',
	        'errors' => $errors,
	        'data' => $message,
	        'email' => $email,
	        'tlds' => $tlds,
	        'title'=>$titlePage,
	    ));
	}

	if (count($errorMessage) == 0) {
		$app['swiftmailer.options'] = array(
                'host' => 'localhost',
                'port' => '25',
                'username' => 'no-reply@khayanganresidence.com',
                'password' => '1q2w3e4r5t6y',
                'encryption' => null,
                'auth_mode' => login
            );
        $message_tin = $app['twig']->render('page/mail.twig', array(
	        'data' => $message,
	        'email' => $email,
			));

        $app['db']->insert('client_email', array(
            'id' => null,
            'sender'=>'no-reply@khayanganresidence.com',
            'recipients'=>'info@khayanganresidence.com',
            'subject'=>'Hi, Email Contact Website Khayangan Residence from '. $message['email'],
            'content_html'=>$message_tin,
            'content_text'=>strip_tags($message_tin),
            'created_at' => 'NOW()',
            'updated_at' => 'NOW()',
        ));


		$pesan = \Swift_Message::newInstance()
	        ->setSubject('Hi, Email Contact Website Khayangan Residence from '. $message['email'])
	        ->setFrom(array('no-reply@khayanganresidence.com'))
	        ->setTo(array('info@khayanganresidence.com', $message['email']))
	        // 'alung@1sthome.co.id', 'hendroks@1sthome.co.id', 
	        ->setBcc( array('fennyhartanto@the-vci.com', 'v1c_hartanto@yahoo.com', 'budi.karyanto71@gmail.com', 'abdillah@khayanganresidence.com')  )
	        ->setBody($message_tin, 'text/html');

	    $app['mailer']->send($pesan);

		return $app->redirect($app['url_generator']->generate('success'));
	}
    

});

$app->get('/success', function () use ($app, $titlePage) {
	$constraint = array(
	    'name' => '',
	    'email' => '',
	    'telp' => '',
		'message' => '',
	);

	return $app['twig']->render('page/success.twig', array(
        'layout' => 'layouts/main.twig',
        'errors' => array(),
        'data' => $constraint,
        'title'=>$titlePage,
    ));
})
->bind('success');

$app['develop'] = array(
				array(
					'img'=>'001.jpg',
					'title'=>'Front Gate Khayangan Residence',
					),
				array(
					'img'=>'002.jpg',
					'title'=>'Jalan Utama Khayangan Residence',
					),
				array(
					'img'=>'003.jpg',
					'title'=>'Marketing Office & Kompleks ruko telah dibangun',
					),
				array(
					'img'=>'004.jpg',
					'title'=>'Pedesterial yang telah dibangun',
					),
				array(
					'img'=>'004b.jpg',
					'title'=>'Pedesterial yang telah dibangun',
					),
				array(
					'img'=>'005.jpg',
					'title'=>'Unit yang telah terjual',
					),
				array(
					'img'=>'006.jpg',
					'title'=>'Pembangunan unit yang telah terjual',
					),
				array(
					'img'=>'007.jpg',
					'title'=>'Pembangunan unit yang telah terjual',
					),
				// interior baru
				array(
					'img'=>'ruby-1.jpg',
					'title'=>'Interior Unit Ruby',
					),
				array(
					'img'=>'ruby-2.jpg',
					'title'=>'Interior Unit Ruby',
					),
				array(
					'img'=>'safir-1.jpg',
					'title'=>'Interior Unit Safir',
					),
				array(
					'img'=>'safir-2.jpg',
					'title'=>'Interior Unit Safir',
					),
				array(
					'img'=>'021-Show-Unit-Khayangan-Residence.jpg',
					'title'=>'Interior Unit Berlian',
					),
				array(
					'img'=>'022-Show-Unit-Khayangan-Residence.jpg',
					'title'=>'Interior Unit Berlian',
					),
				array(
					'img'=>'024-Show-Unit-Khayangan-Residence.jpg',
					'title'=>'Interior Unit Berlian',
					),

				// lama
				array(
					'img'=>'015-Show-Unit-Khayangan-Residence.jpg',
					'title'=>'Show Unit Khayangan Residence',
					),

				array(
					'img'=>'016-Show-Unit-Khayangan-Residence.jpg',
					'title'=>'Show Unit Khayangan Residence',
					),
				array(
					'img'=>'023-Show-Unit-Khayangan-Residence.jpg',
					'title'=>'Show Unit Khayangan Residence',
					),
			);
$app['develop'] = array();

$app['develop'] = array(
	array(
		'img'=>'001.jpg',
		'title'=>'Front Gate Khayangan Residence',
		),
	array(
		'img'=>'Landscape-1.jpg',
		'title'=>'Landscape',
		),
	array(
		'img'=>'Landscape-2.jpg',
		'title'=>'Landscape',
		),
	array(
		'img'=>'Gate-cluster-arjuna.jpg',
		'title'=>'Gate Cluster Arjuna',
		),
	// array(
	// 	'img'=>'Landscape-3.jpg',
	// 	'title'=>'Landscape',
	// 	),
	array(
		'img'=>'Paving-Mahabarata-1.jpg',
		'title'=>'Paving Mahabarata',
		),
	array(
		'img'=>'Paving-Mahabarata-2.jpg',
		'title'=>'Paving Mahabarata',
		),
	array(
		'img'=>'Pedestrian-Mahabarata.jpg',
		'title'=>'Pedestrian Mahabarata',
		),

	// new
	array(
		'img'=>'pedestrian-1.jpg',
		'title'=>'Pedestrian',
		),
	array(
		'img'=>'pedestrian-2.jpg',
		'title'=>'Pedestrian',
		),
	array(
		'img'=>'Pos-Keamanan.jpg',
		'title'=>'Pos Keamanan',
		),

	array(
		// 'img'=>'ruko-ra.jpg',
		'img'=>'Ruko-RA.jpg',
		'title'=>'Ruko RA',
		),
	array(
		// 'img'=>'ruko-re.jpg',
		'img'=>'Ruko-RE.jpg',
		'title'=>'Ruko RE',
		),
	array(
		'img'=>'Rumah-Contoh-45_60.jpg',
		'title'=>'Rumah Contoh 45 60',
		),
	array(
		'img'=>'Rmh-Contoh-Jumeirah.jpg',
		'title'=>'Rumah Contoh Jumeirah',
		),
	);

$app->get('/development/{id}', function($id) use ($app, $titlepage) {

		$id = abs((int) $id);

		// list interior
		$pArrayInterior = array();
		foreach ($app['interior'] as $key => $value) {
			$pArrayInterior[] = array(
								'title'=>$value['folder'],
								'list'=>$value['list'],
							);
		}
		$interior = $pArrayInterior;

		return $app['twig']->render('page/development_popup.twig', array(
	        'layout' => 'layouts/blank.twig',
	        'data' => $app['develop'],
	        'interior' => $interior,
	        'aktif'=>$id,
	        'title'=>$titlePage,
    ));
})
->bind('development');

$app['debug'] = true;

$app->run();