<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl ?>hero-partnership.jpg');">
    <div class="inner_table">
        <div class="prelative container py-5">
            <div class="row py-5">
                <div class="col-md-60 py-5">
                    <div class="insides_intext wow slideInDown text-center">
                        <h3 class="mb-2">Partnership</h3>
                        <div class="py-1"></div>
                        <p>We welcome all probability to grow together</p>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="partner-sec-1 py-5">
    <div class="prelative container py-5">
        <div class="row text-center wow fadeInUp">
            <div class="col-md-60 text-center">
                <div class="content-text">
                    <h5>AMI Colections believes that partnership will strengthen collaborative innovation network to benefit both suppliers, traders, as well as the garment manufacturing factory during the hard times and the good times.</h5>
                    <p>As part of AMI Collections’ culture and strategy of collaborative innovation, we will embrace every opportunity of partnership with all kinds of organisations. For traders who would like to develop OEM / Personal label, to government or private organisations who are looking for quality apparels, for garment related product suppliers who introduce breakthrough materials, we are always here with our arms wide open.</p>
                    <p>Let’s have a quick chat, our staff will respond shortly:</p>
                    <div class="bloc_call_wa">
                        <p class="mb-1">
                            <img src="<?php echo $this->assetBaseurl.'icons-bng-wa.png'; ?>" alt="" class="img img-fluid">
                        </p>
                        <p>Whatsapp Chat <br>
                        <a href="https://wa.me/62818659888">0818 659 888 (Click to chat)</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>