<?php

/**
 * This is the model class for table "tb_stock".
 *
 * The followings are the available columns in table 'tb_stock':
 * @property string $id
 * @property string $kode
 * @property string $name_en
 * @property string $name_id
 * @property string $sizes
 * @property string $contents
 * @property string $harga
 * @property string $harga_coret
 * @property string $stock
 * @property string $satuan
 * @property string $p1_qty
 * @property string $p1_prices
 * @property string $p2_qty
 * @property string $p2_prices
 * @property string $p3_qty
 * @property string $p3_prices
 */
class TbStock extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TbStock the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_stock';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode, harga', 'length', 'max'=>12),
			array('name_en', 'length', 'max'=>33),
			array('name_id', 'length', 'max'=>124),
			array('sizes', 'length', 'max'=>5),
			array('harga_coret', 'length', 'max'=>14),
			array('stock', 'length', 'max'=>4),
			array('satuan', 'length', 'max'=>3),
			array('p1_qty', 'length', 'max'=>10),
			array('p1_prices, p2_prices, p3_prices', 'length', 'max'=>11),
			array('p2_qty', 'length', 'max'=>8),
			array('p3_qty', 'length', 'max'=>9),
			array('contents', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, kode, name_en, name_id, sizes, contents, harga, harga_coret, stock, satuan, p1_qty, p1_prices, p2_qty, p2_prices, p3_qty, p3_prices', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'kode' => 'Kode',
			'name_en' => 'Name En',
			'name_id' => 'Name',
			'sizes' => 'Sizes',
			'contents' => 'Contents',
			'harga' => 'Harga',
			'harga_coret' => 'Harga Coret',
			'stock' => 'Stock',
			'satuan' => 'Satuan',
			'p1_qty' => 'P1 Qty',
			'p1_prices' => 'P1 Prices',
			'p2_qty' => 'P2 Qty',
			'p2_prices' => 'P2 Prices',
			'p3_qty' => 'P3 Qty',
			'p3_prices' => 'P3 Prices',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('name_en',$this->name_en,true);
		$criteria->compare('name_id',$this->name_id,true);
		$criteria->compare('sizes',$this->sizes,true);
		$criteria->compare('contents',$this->contents,true);
		$criteria->compare('harga',$this->harga,true);
		$criteria->compare('harga_coret',$this->harga_coret,true);
		$criteria->compare('stock',$this->stock,true);
		$criteria->compare('satuan',$this->satuan,true);
		$criteria->compare('p1_qty',$this->p1_qty,true);
		$criteria->compare('p1_prices',$this->p1_prices,true);
		$criteria->compare('p2_qty',$this->p2_qty,true);
		$criteria->compare('p2_prices',$this->p2_prices,true);
		$criteria->compare('p3_qty',$this->p3_qty,true);
		$criteria->compare('p3_prices',$this->p3_prices,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}