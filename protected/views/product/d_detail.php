<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl2 ?>hero-products.jpg');">
    <div class="prelative container py-5">
        <div class="py-5"></div>
        <div class="row py-5">
            <div class="col-md-30 py-5">
                <div class="insides_intext">
                    <h3 class="mb-2">PRODUK</h3>
                    <div class="py-1"></div>
                    <p>Ketahui lebih lanjut tentang varian produk pupuk hasil produksi dan pupuk distribusi oleh EKLIN Fertilizer Group.</p>
                    <div class="clear"></div>
                </div>

            </div>
            <div class="col-md-30"></div>
        </div>
    </div>
</section>

<section class="product-sec-1 py-5 back-white">
    <div class="prelative container py-5">
        <div class="inners content-text text-center">
            
            <div class="blocks_product_details">
                <div class="tops_titles">
                    <h5><?php echo $_GET['category'] ?></h5>
                    <div class="py-1"></div>
                    <h2 class="title"><?php echo ucwords( str_replace('-', ' ', $_GET['slug'])) ?></h2>
                </div>
                <div class="py-4 my-2"></div>
                <div class="middles">
                    <div class="row">
                        <div class="col-md-30">
                            <div class="pictures">
                                <img src="https://placehold.it/577x577" alt="" class="img img-fluid">
                            </div>
                        </div>
                        <div class="col-md-30">
                            <div class="d-block d-sm-none py-3"></div>
                            <div class="descriptions text-left">
                                <h6>DESKRIPSI</h6>
                                <p>Dolomit biasa dipergunakan sebagai pupuk pertanian, yang memiliki kandungan hara Kalsium (CaO) dan Magnesium (MgO) tinggi dan sangat bermanfaat untuk pengapuran tanah masam dan dan juga sebagai pupuk bagi tanah dan tanaman yang berfungsi menyuplai unsur Kalsium (CaO) dan Magnesium (MgO) untuk kebutuhan tanaman.</p>
                                
                                <div class="py-2 my-1"></div>
                                <div class="lines-grey"></div>
                                <div class="py-2 my-1"></div>

                                <h6>APLIKASI</h6>
                                <p>Pupuk untuk tanaman kelapa sawit, Pupuk untuk tanaman jagung, Pupuk untuk tanaman aneka buah buahan.</p>

                                <div class="py-2 my-1"></div>
                                <div class="lines-grey"></div>
                                <div class="py-2 my-1"></div>

                                <h6>MANFAAT</h6>
                                <ul><li>Memperbaiki keasaman tanah agar sesuai dengan pH yang diperlukan tanaman</li><li>Menetralkan kejenuhan zat – zat yang meracuni tanah, tanaman, bilamana zat tersebut berlebihan seperti zat Al (alumunium), Fe (zat besi), Cu (Tembaga)</li><li>Meningkatkan efektifitas dan efisiensi penyerapan zat – zat hara yang sudah ada dalam tanah baik yang berasal dari bahan organik maupun pemberian pupuk lainnya seperti Urea, TSP dan Kcl</li><li>Menjaga tingkat ketersediaan unsur hara mikro sesuai kebutuhan tanaman. Artinya dengan Kalsium (CaO) dan Magnesium (MgO) yang cukup unsur mikropun memadai</li><li>Memperbaiki porositas tanah, struktur serta aerasi tanah sekaligus bermanfaat bagi mikrobiologi dan kimiawi tanah sehingga tanah menjadi gembur dsn sirkulasi udara dalam tanah lancar</li><li>Aktifator berbagai jenis enzim tanaman, merangsang pembentukan senyawa lemak dan minyak, serta karbohidrat</li><li>Membantu translokasi pati dan distribusi phospor didalam tubuh tanaman</li><li>Unsur pembentuk warna daun (Klorofil), sehingga tercipta hijau daun yang sempurna</li></ul>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>

            <div class="py-2"></div>
            <div class="clear"></div>
        </div>
    </div>
</section>
