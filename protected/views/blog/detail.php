<div class="spacers_inside"></div>

<section class="product-sec-1 py-5">
    <div class="prelative container py-5">

        <div class="tops_content mb-4 pb-3 text-center">
            <h2>Our News & Articles</h2>
            <div class="clear"></div>
        </div>

        <div class="middles_content boxeds_blog_detail">
            <div class="py-2"></div>
            <div class="picture"><img src="<?php echo $this->assetBaseurl ?>blogs_exam.jpg" alt="" class="img img-fluid w-100"></div>

            <div class="py-2 my-2"></div>

            <div class="info">
              <div class="row">
                <div class="col-md-50">
                  <h1 class="title-article">Judul Artikel Di sini</h1>
                </div>
                <div class="col-md-10">
                  <div class="text-right backs_collect">
                        <a href="#">Back to index</a>
                    </div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel dui non felis hendrerit aliquet. Vivamus congue, ligula eu aliquet blandit, nibh erat interdum nunc, pellentesque ullamcorper dui mi non diam. Sed euismod, magna non tempor pulvinar, turpis libero lobortis mauris, a placerat ex elit ut neque. Phasellus faucibus, tortor ac hendrerit molestie, libero libero suscipit nibh, et tincidunt sapien nisi nec ipsum. Duis eu odio a lectus congue pellentesque. Sed in eros tempor, ornare diam mollis, laoreet ipsum. Proin facilisis aliquet risus sed eleifend. Donec iaculis risus vitae nisl tincidunt, suscipit auctor arcu placerat. Praesent tempus pellentesque eros, at ultrices tortor ornare non. Pellentesque dolor justo, rhoncus eget vestibulum in, consectetur at risus. Praesent molestie lacus non leo lobortis varius ac eget dui.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel dui non felis hendrerit aliquet. Vivamus congue, ligula eu aliquet blandit, nibh erat interdum nunc, pellentesque ullamcorper dui mi non diam. Sed euismod, magna non tempor pulvinar, turpis libero lobortis mauris, a placerat ex elit ut neque. Phasellus faucibus, tortor ac hendrerit molestie, libero libero suscipit nibh, et tincidunt sapien nisi nec ipsum. Duis eu odio a lectus congue pellentesque. Sed in eros tempor, ornare diam mollis, laoreet ipsum. Proin facilisis aliquet risus sed eleifend. Donec iaculis risus vitae nisl tincidunt, suscipit auctor arcu placerat. Praesent tempus pellentesque eros, at ultrices tortor ornare non. Pellentesque dolor justo, rhoncus eget vestibulum in, consectetur at risus. Praesent molestie lacus non leo lobortis varius ac eget dui.</p>
              <div class="py-1"></div>

              <div class="row">
                <div class="col-md-30">
                  <div class="text-left backs_collect">
                      <a href="#">Previous Article</a>
                  </div>
                </div>
                <div class="col-md-30">
                  <div class="text-right backs_collect">
                      <a href="#">Next Article</a>
                  </div>
                </div>
              </div>
              <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>

    </div>
</section>

<section class="product-sec-2 py-5">
    <div class="prelative container py-3">

        <div class="tops_other content-text">
            <div class="row">
                <div class="col-md-30">
                    <h5 class="m-0 mb-0">Other News & Articles</h5>
                </div>
                <div class="col-md-30">
                    <div class="py-1"></div>
                    <div class="text-right backs_collect">
                        <a href="#">Back to index</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-3"></div>

        <!-- Start list blog -->
        <div class="lists_blogs_set">
            <div class="row">
                <?php for ($i=1; $i < 4; $i++) { ?>
                <div class="col-md-20 col-sm-30">
                    <div class="items">
                        <div class="pict">
                            <a href="#"><img src="<?php echo $this->assetBaseurl ?>blogs_exam.jpg" alt="" class="img img-fluid"></a>
                            </div>
                        <div class="info">
                            <a href="#"><h4>Article title over here</h4></a>
                            <a href="#" class="btn btn-link p-0 link-blogs">Read Article</a>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <!-- End list blog -->

        <div class="clear"></div>
    </div>
</section>




<?php 
/*
<section class="blog-sec-1 py-5 back-white">
    <div class="prelative container py-5">
        <div class="inners content-text text-left">

            <div class="outers_blog_details wow fadeInUp">
              <h1 class="mb-4"><?php echo $dataBlog->description->title ?></h1>
              <div class="pict"><img src="<?php echo $this->assetBaseurl.'../../images/blog/'; ?><?php echo $dataBlog->image ?>" alt="" class="img img-fluid w-100"></div>
              <div class="contents content-text pt-4">
                <?php echo $dataBlog->description->content; ?>
              </div>
              <div class="clear"></div>
            </div>
            <div class="py-5"></div>

            <?php if ($dataBlogs): ?>
            <h3>Other Blog</h3>
            <div class="py-2"></div>
            <div class="lists_data_fx_blog wow fadeInUp">
              <div class="row">
                <?php foreach ($dataBlogs->getData() as $key => $value){ ?>
                <div class="col-md-30">
                  <div class="items">
                    <div class="picture">
                      <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id )); ?>"><img class="img img-fluid w-100"src="<?php echo $this->assetBaseurl.'../../images/blog/'; ?><?php echo $value->image ?>" alt=""></a>
                    </div>
                    <div class="info py-3">
                      <div class="py-1"></div>
                      <span class="dates"><?php echo date('d F Y', strtotime($value->date_input)); ?></span>
                      <div class="py-1"></div>
                      <h2 class="title"><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id )); ?>"><?php echo ucwords($value->description->title); ?></a></h2>
                      <div class="py-2"></div>
                      <p><?php echo substr(strip_tags($value->description->content), 0, 100).'...'; ?></p>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php endif; ?>

            <div class="clear"></div>
        </div>
    </div>
</section>
*/ ?>