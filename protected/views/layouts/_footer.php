<footer class="foot">
    <div class="prelatife container cont-footer">
        <div class="row">
            <div class="col-md-15">
                <div class="lgo-footers"><a href="#"><img src="<?php echo $this->assetBaseurl.'lgosn_foot.png'; ?>" alt="" class="img img-fluid"></a></div>
            </div>
            <div class="col-md-45">
                <ul class="list-inline m-0 text-left menus_foot">
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">OUR COMPANY</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">OUR PRODUCTS</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/quality')); ?>">OUR QUALITY</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/partner')); ?>">PARTNERSHIP</a></li>
                    <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">CONTACT</a></li>
                </ul>
            </div>
        </div>
        <div class="py-2 my-1"></div>
        <div class="lines-white"></div>
        <div class="py-2 my-1"></div>
        <div class="row">
            <div class="col-md-15">
                <p>&copy; AMI Collection 2020 <br>
                Garment - Clothing convection business.</p>
            </div>
            <div class="col-md-30">
                <p class="def"><b>PT. Aneka Mode Indonesia</b> <br>
                Jl. Kidemang Singomenggolo No.1-2, Sidomulyo, Kec. Sidoarjo, Jawa Timur, 61252 Telephone. (031) 8050387</p>
            </div>
            <div class="col-md-15">
                <div class="b_social row no-gutters">
                    <div class="col-8">
                        <img src="<?php echo $this->assetBaseurl.'icons_n_head.png'; ?>" alt="" class="img img-fluid">
                    </div>
                    <div class="col">
                        <p class="def"><b>Whatsapp Chat</b><br>
                        <a href="https://wa.me/62818659888">0818 659 888 (Click to chat)</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-3"></div>
        <div class="t-copyrights text-left">
            <p>Website design by <a target="_blank" title="Website Design Surabaya" href="https://markdesign.net">Mark Design Indonesia.</a></p>
        </div>
    </div>
</footer>


<section class="live-chat">
    <div class="row">
        <div class="col-md-60">
            <div class="live">
                <a href="https://wa.me/62818659888">
                    <img src="<?php echo $this->assetBaseurl; ?>wa-btns_block.png" class="img img-fluid" alt="">
                </a>
            </div>
        </div>
    </div>
</section>

<section class="nfoot_mob_wa d-block d-sm-none">
    <div class="row">
        <div class="col-md-60">
            <div class="nx_button">
                <a href="https://wa.me/62818659888"><i class="fa fa-whatsapp"></i> Whatsapp</a>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    .imgs_vold img{
        max-width: 255px;
    }
    .live-chat .live img{
        max-width: 175px;
    }
</style>