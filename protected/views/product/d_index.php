<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl2 ?>hero-products.jpg');">
    <div class="prelative container py-5">
        <div class="py-5"></div>
        <div class="row py-5">
            <div class="col-md-30 py-5">
                <div class="insides_intext wow slideInLeft">
                    <h3 class="mb-2">PRODUK</h3>
                    <div class="py-1"></div>
                    <p>Ketahui lebih lanjut tentang varian produk pupuk hasil produksi dan pupuk distribusi oleh EKLIN Fertilizer Group.</p>
                    <div class="clear"></div>
                </div>

            </div>
            <div class="col-md-30"></div>
        </div>
    </div>
</section>

<section class="product-sec-1 py-5 back-white">
    <div class="prelative container py-5">
        <div class="inners content-text text-center">
            
            <?php 
            $data_pupuk = [
                            'Pupuk_NPK' => [
                                            [
                                                'name'=>'PUPUK NPK Blending',
                                                'pict'=>'cover-npk.jpg',
                                                'info'=>'<p><strong>Merk Bianglala:</strong><br>12-6-22-3-TE, 12-12-17-2, 13-8-27-4MgO - 0,5B, 13-8-27-4MgO - 0,65B, 15-15-15, 15-15-6-4, 16-16-16, 20-10-10</p>',
                                            ],
                                            [
                                                'name'=>'PUPUK NPK COMPOUND',
                                                'pict'=>'cover-npk-compound.jpg',
                                                'info'=>'<p><strong>Merk Unta (Import)</strong></p>',
                                            ],
                                          ],

                            'Pupuk_PHOSPAT' => [
                                            [
                                                'name'=>'PUPUK PHOSPAT PSP 36 & AGROFOS 36',
                                                'pict'=>'cover-phosphat.jpg',
                                                'info'=>'<p><strong>Merk Sawah Subur, Roda Cahaya</strong></p>',
                                            ], 
                                          ],

                            'Pupuk_DOLOMIT' => [
                                            [
                                                'name'=>'PUPUK DOLOMIT SUPER',
                                                'pict'=>'cover-dolomit.jpg',
                                                'info'=>'<p><strong>Merk Kereta Mas</strong></p>',
                                            ], 
                                          ],

                            'Pupuk_KCL_(Kalium_Klorida)' => [
                                            [
                                                'name'=>'Pupuk KCL Granul & Powder',
                                                'pict'=>'cover-kcl.jpg',
                                                'info'=>'<p><strong>Merk Unta</strong></p>',
                                            ], 
                                          ],
                            'Pupuk_Pembenah_Tanah' => [
                                            [
                                                'name'=>'Pupuk Agrofos 18',
                                                'pict'=>'cover-pembenah-tanah.jpg',
                                                'info'=>'<p><strong>Merk Agrofos 18</strong></p>',
                                            ], 
                                          ],
                                          
                          ];
            ?>
            <div class="blocks_top_category product_pg text-center wow fadeInUp">
                <h3>Kategori Produk Pupuk</h3>
                <div class="py-3"></div>
                <ul class="list-inline">
                    <li class="list-inline-item"><a class="toscroll" data-id="n_prd_0" href="#">PUPUK NPK</a></li>
                    <li class="list-inline-item"><a class="toscroll" data-id="n_prd_1" href="#">PUPUK PHOSPAT</a></li>
                    <li class="list-inline-item"><a class="toscroll" data-id="n_prd_2" href="#">PUPUK DOLOMIT</a></li>
                    <li class="list-inline-item"><a class="toscroll" data-id="n_prd_3" href="#">PUPUK KCL (Kalium Klorida)</a></li>
                    <li class="list-inline-item"><a class="toscroll" data-id="n_prd_4" href="#">PUPUK Pembenah Tanah</a></li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="py-3 my-1"></div>
            <div class="lines-grey"></div>
            <div class="py-4 my-2"></div>


            <div class="blocks_listproduct_categorys product_pg">
                <?php $nums = 0; ?>
                <?php foreach ($data_pupuk as $key => $value): ?>
                <div id="n_prd_<?php echo $nums ?>" class="blocks_list">
                    <div class="titles wow fadeInUp"><?php echo (str_replace('_', ' ', $key)); ?></div>
                    <div class="py-3 my-1"></div>
                    <div class="row lists_data_categorys wow fadeInUp">
                        <?php foreach ($value as $ke => $val): ?>
                        <div class="col-md-60">
                            <div class="items text-center mb-5">
                                <!-- <a href="<?php // echo CHtml::normalizeUrl(array('/product/d_detail', 'slug' => Slug::Create($val['name']), 'category'=> (str_replace('_', ' ', $key)) )); ?>"></a> -->
                                <h4><?php echo $val['name'] ?></h4>
                                <div class="py-2 my-1"></div>
                                <div class="picture">
                                    <img src="<?php echo $this->assetBaseurl2 . $val['pict'] ?>" alt="" class="img img-fluid w-100">
                                </div>
                                <div class="info pb-2">
                                    <?php echo $val['info'] ?>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>
                    </div>
                </div>

                <div class="py-3"></div>
                <div class="lines-grey"></div>
                <div class="py-3"></div>
                <?php $nums = $nums+1; ?>
                <?php endforeach ?>

                <div class="clear"></div>
            </div>            
            <div class="py-2"></div>

            <div class="clear"></div>
        </div>
    </div>
</section>
