<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl ?>hero-contact.jpg');">
    <div class="inner_table">
        <div class="prelative container py-5">
            <div class="py-5"></div>
            <div class="row py-5">
                <div class="col-md-60 py-5">
                    <div class="insides_intext wow slideInDown text-center">
                        <h3 class="mb-2">Contact</h3>
                        <div class="py-1"></div>
                        <p>We welcome all probability to grow together</p>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-sec-1 py-5">
    <div class="prelative container py-5">
        <div class="row text-center wow fadeInUp">
            <div class="col-md-60 text-center">
                <div class="content-text">
                    <p>Let’s schedule your site visit or simply chat with our customer service:</p>
                    <div class="py-2"></div>
                    <div class="bloc_call_wa">
                        <p class="mb-1">
                            <img src="<?php echo $this->assetBaseurl.'icons-bng-wa.png'; ?>" alt="" class="img img-fluid">
                        </p>
                        <p>Whatsapp Chat <br>
                        <a href="https://wa.me/62818659888">0818 659 888 (Click to chat)</a></p>
                    </div>

                    <div class="py-4"></div>

                    <div class="contacts_ond">
                        <p><b>PT. Aneka Mode Indonesia</b><br>
                        Jl. Kidemang Singomenggolo No.1-2, Sidomulyo, Kec. Sidoarjo, Jawa Timur, 61252 <br>
                        <a class="view_map" href="#">View on Google Map</a></p>
                        <p><b>Telephone.</b> (031) 8050387 - <b>Email.</b> info@amicollection.com</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</section>