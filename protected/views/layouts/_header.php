<?php 
$active_menu_pg = $this->id.'/'.$this->action->id;
?>
<header class="head headers fixed-top <?php if ($active_menu_pg == 'home/index'): ?>homes_head<?php endif ?> ">
  <div class="prelative d-none d-sm-block">
    <div class="blues_head">
      <div class="row no-gutters">
        <div class="col-md-12 bg-white">
          <div class="logo_heads py-4 my-2">
            <div class="d-block mx-auto text-center">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                <img data-inside-src="<?php echo $this->assetBaseurl; ?>logo-headers-in.png" alt="<?php echo Yii::app()->name; ?>" data-home-src="<?php echo $this->assetBaseurl; ?>logo-headers.png" alt="<?php echo Yii::app()->name; ?>" src="<?php echo $this->assetBaseurl; ?>logo-headers-in.png" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid">
              </a>
            </div>

          </div>
        </div>

        <div class="col-md-30 my-auto">

          <div class="text-center top-menu pt-3 mt-3 d-block">
            <ul class="list-inline">
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">OUR COMPANY</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">OUR PRODUCTS</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/quality')); ?>">OUR QUALITY</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/partner')); ?>">PARTNERSHIP</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">CONTACT</a></li>
            </ul>
          </div>

          <div class="clear clearfix"></div>
        </div>
        <div class="col-md-15 my-auto">
          <div class="info_head pt-3 mt-3 d-block">
            <p class="wa_call float-left ml-5">WHATSAPP &nbsp;&nbsp;<a href="https://wa.me/62818659888"><img src="<?php echo $this->assetBaseurl ?>icons_n_head.png" alt="" class="img img-fluid">&nbsp;&nbsp; 0818 659 888</a></p>
            <div class="change_lang float-right">
              <a href="#">ENG</a>
              &nbsp;&nbsp;|&nbsp;&nbsp;
              <a href="#">IN</a>
            </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>

  </div>

  <div class="d-block d-sm-none">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
        <img src="<?php echo $this->assetBaseurl.'logo-headers-in.png' ?>" style="max-width: 210px;" alt="" class="img img-fluid">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">OUR COMPANY</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">OUR PRODUCTS</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/quality')); ?>">OUR QUALITY</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/partner')); ?>">PARTNERSHIP</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">CONTACT</a></li>
        </ul>
      </div>
    </nav>
  </div>

</header>