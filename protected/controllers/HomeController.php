<?php

class HomeController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}

	public function actionCreatecategory()
	{
		for ($i=1; $i < 111; $i++) { 
			$model = new PrdCategoryProduct;
			$model->category_id = 12;
			$model->product_id = $i;
			$model->save(false);
		}
	}

	public function actionInput()
	{
		echo "has been uploads"; exit;
		$data = TbStock::model()->findAll();
		foreach ($data as $key => $value) {

			$model = PrdProduct::model()->find('kode = :kode', array( ':kode'=> $value->kode ));
			if ($model == null) {
				$model = new PrdProduct;
			}

			$model->kode = $value->kode;
			if ($model->kode) {
				// $model->image = $model->kode.'.jpg';
			}else{
				// $model->image = '';
			}

			// $model->category_id = 6;
			// $model->stock = $value->stock;
			// $model->satuan = $value->satuan;
			// $model->sizes = $value->sizes;
			// $model->stock = $value->stock;
			// $model->harga = $value->harga;
			// $model->harga_coret = $value->harga_coret;
			// $model->data = 's:6:"a:0:{}";';
			// $model->tag = 'New Born & Baby Clothing, Hansop';
			$model->p1_qty = $value->p1_qty;
			$model->p1_prices = $value->p1_prices;
			$model->p2_qty = $value->p2_qty;
			$model->p2_prices = $value->p2_prices;
			$model->p3_qty = $value->p3_qty;
			$model->p3_prices = $value->p3_prices;
			$model->save(false);

			// $dataDesc = new PrdProductDescription;
			// $dataDesc->product_id = $model->id;
			// $dataDesc->language_id = 2;
			// $dataDesc->name = $value->name_en;
			// $dataDesc->desc = '<p>'.$value->contents.'</p>';
			// $dataDesc->save(false);

			// $dataDesc2 = new PrdProductDescription;
			// $dataDesc2->product_id = $model->id;
			// $dataDesc2->language_id = 3;
			// $dataDesc2->name = $value->name_id;
			// $dataDesc2->desc = '<p>'.$value->contents.'</p>';
			// $dataDesc2->save(false);

			// $prdImage = new PrdProductImage;
			// $prdImage->product_id = $model->id;
			// $prdImage->image = 'ddf15-Untitled-1.jpg';
			// $prdImage->save(false);
		}

		echo "data has been input";
	}


	public function actionIndex()
	{
		$this->layout='//layouts/column1';

		$this->render('index', array(
		));
	}

	public function actionError()
	{
		// $this->layout = '//layouts/error';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else{
				
				$this->layout='//layouts/column2';

				$this->pageTitle = 'Error '.$error['code'].': '. $error['message'] .' - '.$this->pageTitle;

				$this->render('error', array(
					'error'=>$error,
				));
			}
		}

	}

	public function actionAbout()
	{
		$this->pageTitle = 'About Us - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('about', array(	
		));
	}

	public function actionQuality()
	{
		$this->pageTitle = 'Quality - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('quality', array(	
		));
	}

	public function actionPartner()
	{
		$this->pageTitle = 'Quality - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('partner', array(	
		));
	}

	public function actionCareer()
	{
		$this->pageTitle = 'Career - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('career', array(	
		));
	}
	
	public function actionContact()
	{
		$this->layout='//layouts/column2';

		$this->pageTitle = 'Contact - '.$this->pageTitle;

		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			$status = true;
	        $secret_key = "6LewK0EUAAAAAKBkAiXhXsR1ELPt7mQK5mcPRll2";
	        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        $response = json_decode($response);
	        if($response->success==false)
	        {
	          $status = false;
	          $model->addError('verifyCode', 'Verify you are not robbot');
	        }

			if($status AND $model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email']),
					'subject'=>'['.Yii::app()->name.'] Contact from '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}

		$this->render('contact2', array(
			'model'=>$model,
		));
	}

}


