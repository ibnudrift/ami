<section class="insidespg-cover py-5" style="background-image: url('<?php echo $this->assetBaseurl ?>hero-about.jpg');">
    <div class="inner_table">
        <div class="prelative container py-5">
            <div class="py-5"></div>
            <div class="row py-5">
                <div class="col-md-60 py-5">
                    <div class="insides_intext wow slideInDown text-center">
                        <h3 class="mb-2">Our Company</h3>
                        <div class="py-1"></div>
                        <p>Ami Collection, the name that invites you to grow and collaborate with limitless expansions.</p>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-sec-1 py-5">
    <div class="prelative container py-5">
        <div class="row text-center wow fadeInUp">
            <div class="col-md-60 text-center">
                <div class="content-text">
                    <h5>AMI Colection adalah perusahaan konveksi yang didirikan sejak tahun 1983 dan dimulai dari nol. Kami berkembang sedikit demi sedikit dan menemukan pasar yang terus menerus memberikan kepercayaan kepada kami. Bermula dari konveksi / pembuatan pakaian untuk bayi yang ditawarkan secara tradisional ke agen dan distributor pakaian, hingga saat ini AMI Collection telah menjalin hubungan eksklusif dengan berbagai department store eksklusif yang ada di Indonesia untuk menjadi pemasok eksklusif, terkontrak dan berkelanjutan.</h5>
                    <p>Seiring waktu berjalan, AMI Collection pun mulai merambah pengembangan divisi untuk melayani kebutuhan yang berhubungan dengan dunia kesehatan, yakni dalam produksi masker kain non medis dan berbagai turunan variannya, serta pengembangan divisi custom apparel / seragam, hingga dengan spesifikasi yang sangat rumit sekalipun. Keahlian AMI Collection yang panjang di dunia pakaian bayi dan anak menjadi bekal yang baik dalam pengembangan usahanya. Seluruh produk yang dihasilkan selalu mengutamakan kenyamanan pemakian terutama dalam waktu yang lama, kelembutan bahan yang bersentuhan dengan kulit serta potongan pola yang memudahkan pergerakan dalam rutinitas yang sesuai dengan peran pemakai.</p>
                    <p>Dengan tekad untuk terus maju dan berkembang, AMI Collection sangat terbuka untuk segala kemungkinan dalam kerjasama dan saling mengembangkan usaha bersama. Kami akan mewujudkan ide terbaik anda, menghadirkannya dalam bentuk yang profesional dan memiliki nilai lebih.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-sec-2">
    <div class="prelative container">
        <div class="inners text-center wow fadeInUp content-text">
            <div class="py-5"></div>
            <h2 class="def-title">Our Culture</h2>
            <div class="py-4"></div>
            <div class="list-our-cultures-abt text-center">
                <div class="row">
                    <div class="col-md-20">
                        <div class="items">
                            <div class="pict"><img src="<?php echo $this->assetBaseurl.'icon-cultures-1.png'; ?>" alt="" class="img img-fluid d-block mx-auto"></div>
                            <div class="infos">
                                <h5>We are inspiring</h5>
                                <p>We craft and innovate ideas that delight, surprise and always bring value and efficiency.</p>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-20">
                        <div class="items">
                            <div class="pict"><img src="<?php echo $this->assetBaseurl.'icon-cultures-2.png'; ?>" alt="" class="img img-fluid d-block mx-auto"></div>
                            <div class="infos">
                                <h5>We are challenging ourselves</h5>
                                <p>We perform with a mindset of best and see challenges as opportunities to grow. We anticipate what’s next, question conventions and welcome debate.</p>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-20">
                        <div class="items">
                            <div class="pict"><img src="<?php echo $this->assetBaseurl.'icon-cultures-3.png'; ?>" alt="" class="img img-fluid d-block mx-auto"></div>
                            <div class="infos">
                                <h5>We act with heart and soul</h5>
                                <p>We establish true mutual partnerships, because we are dedicated, passionate and always give our all. We take responsibility for our work.</p>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End list our culture -->
            <div class="d-none d-sm-block py-5"></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="d-block d-sm-none">
        <img src="<?php echo $this->assetBaseurl ?>about-backs_nmiddles_mobiles.jpg" alt="" class="img img-fluid">
    </div>
</section>

<section class="about-sec-3 py-5">
    <div class="prelative container py-4">
        <div class="inners text-center wow fadeInUp content-text">
            <h2 class="def-title">Vision & Mission</h2>
            <div class="py-4 d-none d-sm-block"></div>

            <div class="conts_about">
                <div class="row">
                    <div class="col-md-30 brn_right">
                        <div class="py-1"></div>
                        <p>To be the company that inspire emotion through our designs and creations, the world-class convection factory  that sets the benchmark of quality that everyone looked up to</p>
                        <div class="py-1"></div>
                    </div>
                    <div class="col-md-30">
                        <div class="py-1"></div>
                        <p>To create new breakthrough design everyday, introducing new probabilities from the development of “design that meets function”</p>
                        <div class="py-1"></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>

            <div class="py-3"></div>

            <div class="clear"></div>
        </div>
    </div>
</section>