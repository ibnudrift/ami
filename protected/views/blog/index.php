<div class="spacers_inside"></div>

<section class="product-sec-1 py-5">
    <div class="prelative container py-5">

        <div class="tops_content mb-4 pb-3 text-center">
            <h2>Our News & Articles</h2>
            <div class="clear"></div>
        </div>

        <div class="middles_content">
            <div class="py-2"></div>

            <!-- Start list blog -->
            <div class="lists_blogs_set">
                <div class="row">
                    <?php for ($i=1; $i < 13; $i++) { ?>
                    <div class="col-md-15 col-sm-30">
                        <div class="items">
                            <div class="pict">
                                <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail')); ?>"><img src="<?php echo $this->assetBaseurl ?>blogs_exam.jpg" alt="" class="img img-fluid"></a>
                                </div>
                            <div class="info">
                                <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail')); ?>"><h4>Article title over here</h4></a>
                                <a href="#" class="btn btn-link p-0 link-blogs">Read Article</a>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!-- End list blog -->
            <div class="py-2"></div>
            
            <nav aria-label="Page navigation example">
              <ul class="pagination justify-content-center  pagination-sm">
                <li class="page-item disabled">
                  <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                  <a class="page-link" href="#">Next</a>
                </li>
              </ul>
            </nav>

            <div class="clear"></div>
        </div>

    </div>
</section>


<?php /*
<section class="blog-sec-1 py-5 back-white">
    <div class="prelative container py-5">
        <div class="inners content-text text-left">

            <?php if ($dataBlog): ?>
            <div class="lists_data_fx_blog">
              <div class="row">
                <?php foreach ($dataBlog->getData() as $key => $value){ ?>
                <div class="col-md-30 wow fadeInUp">
                  <div class="items">
                    <div class="picture">
                      <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id )); ?>"><img class="img img-fluid w-100"src="<?php echo $this->assetBaseurl.'../../images/blog/'; ?><?php echo $value->image ?>" alt=""></a>
                    </div>
                    <div class="info py-3">
                      <div class="py-1"></div>
                      <span class="dates"><?php echo date('d F Y', strtotime($value->date_input)); ?></span>
                      <div class="py-1"></div>
                      <h2 class="title"><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id )); ?>"><?php echo ucwords($value->description->title); ?></a></h2>
                      <div class="py-2"></div>
                      <p><?php echo substr(strip_tags($value->description->content), 0, 100).'...'; ?></p>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php endif; ?>

            <?php 
               $this->widget('CLinkPager', array(
                  'pages' => $dataBlog->getPagination(),
                  'header'=>'',
                  'footer'=>'',
                  'lastPageCssClass' => 'd-none',
                  'firstPageCssClass' => 'd-none',
                  'nextPageCssClass' => 'd-none',
                  'previousPageCssClass' => 'd-none',
                  'itemCount'=> $dataBlog->totalItemCount,
                  'htmlOptions'=>array('class'=>'pagination'),
                  'selectedPageCssClass'=>'active',
              ));
           ?>

            <div class="clear"></div>
        </div>
    </div>
</section>
*/ ?>