<div class="spacers_inside"></div>

<section class="home-section-1 bg-white py-5">
    <div class="prelatife container">
        <div class="inner py-5 homes-text text-center">
            
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-54">
                    <h2 class="title-def">Our Products</h2>
                    <div class="py-2"></div>
                    <p>Decades of commitment to garment related products have enabled AMI Collection to predict and create styles of various layers of consumers. With this knowledge, AMI Collections’ products are favourite in the market and undoubtedly the playmaker in the big city markets such as Tanah Abang Jakarta, Mangga Dua Jakarta, and other big markets in Surabaya, Jogjakarta, Semarang, Bali, and so much more.</p>
                </div>
                <div class="col-md-3"></div>
            </div>

            <div class="py-4 my-1"></div>

            <?php 
            $criteria = new CDbCriteria;
            $criteria->with = array('description');
            $criteria->addCondition('t.parent_id = "0"');
            $criteria->addCondition('t.type = :type');
            $criteria->params[':type'] = 'category';
            $criteria->limit = 3;
            $criteria->order = 't.sort ASC';
            $strCategory_all = PrdCategory::model()->findAll($criteria);
            ?>
            <div class="lists-def-banners-product">
                <div class="row">
                    <?php foreach ($strCategory_all as $key => $value): ?>
                    <div class="col-md-20">
                        <div class="items prelatife">
                            <div class="picture">
                                <img src="<?php echo $this->assetBaseurl.'../../images/category/'. $value->image2 ?>" alt="" class="img img-fluid">
                            </div>
                            <div class="info">
                                <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=> $value->id, 'slug'=> Slug::Create($value->description->name) )); ?>"><h4><?php echo $value->description->name ?></h4></a>
                                <p><?php echo $value->description->description ?></p>
                                <div class="py-2"></div>
                                <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=> $value->id, 'slug'=> Slug::Create($value->description->name) )); ?>" class="btn btn-link btns_bdefaults">View Collections</a>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>                    
                </div>
            </div>


            <div class="clear clearfix"></div>
        </div>
    </div>
</section>