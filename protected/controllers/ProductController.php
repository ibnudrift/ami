<?php

class ProductController extends Controller
{

	public $product, $category;

	public function actionLanding()
	{
		$this->pageTitle = 'Our Products - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('product_landing', array(	
		));
	}
	
	public function actionIndex()
	{
		$criteria2=new CDbCriteria;
		$criteria2->with = array('description', 'category', 'categories');
		$criteria2->order = 't.date_input ASC';
		$criteria2->addCondition('status = "1"');
		$criteria2->addCondition('description.language_id = :language_id');
		$criteria2->params[':language_id'] = $this->languageID;
		if ($_GET['q'] != '') {
			$criteria2->addCondition('t.filter LIKE :q OR description.name LIKE :q OR description.desc LIKE :q');
			$criteria2->params[':q'] = '%'.$_GET['q'].'%';
		}

		if ($_GET['category']) {
			$criteria = new CDbCriteria;
			$criteria->with = array('description');
			$criteria->addCondition('t.id = :id');
			$criteria->params[':id'] = $_GET['category'];
			$criteria->addCondition('t.type = :type');
			$criteria->params[':type'] = 'category';
			// $criteria->limit = 3;
			$criteria->order = 'sort ASC';
			$strCategory = PrdCategory::model()->find($criteria);

			$inArray = PrdProduct::getInArrayCategory($_GET['category']);
			$criteria2->addInCondition('t.category_id', $inArray);
			// $criteria2->addCondition('t.tag LIKE :category');
			// $criteria2->params[':category'] = '%category='.$_GET['category'].',%';

		}
		if ($strCategory !== null) {
			if ($strCategory->parent_id > 0) {
				$criteria = new CDbCriteria;
				$criteria->with = array('description');
				$criteria->addCondition('t.id = :id');
				$criteria->params[':id'] = $strCategory->parent_id;
				$criteria->addCondition('t.type = :type');
				$criteria->params[':type'] = 'category';
				// $criteria->limit = 3;
				$criteria->order = 'sort ASC';
				$strParentCategory = PrdCategory::model()->find($criteria);

				// Filter By Category
				// $criteria2->addCondition('t.category_id = :category_id');
				// $criteria2->params[':category_id'] = $_GET['category'];
			}else{
				// Filter By Parent
				// $criteria2->addCondition('category.parent_id = :parent_id');
				// $criteria2->params[':parent_id'] = $_GET['category'];
			}
		}

		$strChildCategory = array();
		if ($strParentCategory != null) {
			$criteria = new CDbCriteria;
			$criteria->with = array('description');
			$criteria->addCondition('t.parent_id = :parents_id');
			$criteria->params[':parents_id'] = $strCategory->parent_id;
			$criteria->addCondition('t.type = :type');
			$criteria->params[':type'] = 'category';
			// $criteria->limit = 3;
			$criteria->order = 'sort ASC';
			$CategorySubm = PrdCategory::model()->findAll($criteria);
		}else{
			$criteria = new CDbCriteria;
			$criteria->with = array('description');
			$criteria->addCondition('t.parent_id = :parents_id');
			$criteria->params[':parents_id'] = $_GET['category'];
			$criteria->addCondition('t.type = :type');
			$criteria->params[':type'] = 'category';
			// $criteria->limit = 3;
			$criteria->order = 'sort ASC';
			$strChildCategory = PrdCategory::model()->findAll($criteria);
		}

		$criteria3 = $criteria2;
		$criteria3->select = "t.id, t.brand_id";
		$criteria3->group = 't.id';
		$listProductId = PrdProduct::model()->findAll($criteria3);
		
		$idBrand = array();
		foreach ($listProductId as $key => $value) {
			array_push($idBrand, $value->brand_id);
		}
		$idBrand = array_unique($idBrand);
		
		// $criteria4 = new CDbCriteria;
		// $criteria4->with = array('description');
		// $criteria4->addCondition('active = "1"');
		// $criteria4->addCondition('description.language_id = :language_id');
		// $criteria4->params[':language_id'] = $this->languageID;
		// $criteria4->addInCondition('t.id', $idBrand);
		// $dataBrand = Brand::model()->findAll($criteria4);

		$idProduct = array();
		foreach ($listProductId as $key => $value) {
			array_push($idProduct, $value->id);
		}
		$idProduct = array_unique($idProduct);
		$typeLabel = PrdCategory::typeList($idProduct, $this->languageID);

		// ------------------ FILTER --------------------------
		$get = $_GET;
		unset($get['order']);
		unset($get['category']);
		$no = 1;
		foreach ($get as $key => $val) {
			$sql = array();
			if (is_array($get[$key])) {
				foreach ($get[$key] as $value) {
					$sql[] = 't.filter LIKE :filter'.$no;
					$criteria2->params[':filter'.$no] = '%'.$key.'='.$value.'%';
					$no++;
				}
				$criteria2->addCondition(implode(' OR ', $sql));
			}
		}


		// ------------------ ORDER ---------------------------
		switch ($_GET['order']) {
			case 'low-price':
				$criteria2->order = 't.harga ASC';
				break;

			case 'hight-price':
				$criteria2->order = 't.harga DESC';
				break;
			
			default:
				$criteria2->order = 't.date DESC';
				break;
		}
		
		if ($_GET['page_size'] != '') {
			$pageSize = $_GET['page_size'];
		}else{
			$pageSize = 15;
			
		}
		$criteria2->select = "*";
		$criteria2->group = 't.id';
		$product = new CActiveDataProvider('PrdProduct', array(
			'criteria'=>$criteria2,
		    'pagination'=>array(
		        'pageSize'=>$pageSize,
		    ),
		));

		$this->layout='//layouts/column2';
		$this->pageTitle = $strCategory->description->name. (($strParentCategory != null) ? ' - '.$strParentCategory->description->name.' - ' : ' ' ).$this->pageTitle;

		// index_list
		$this->render('product_view', array(
			'product'=>$product,
			'strCategory'=>$strCategory,
			'CategorySubm'=>$CategorySubm,
			'strParentCategory'=>$strParentCategory,
			'strChildCategory'=>$strChildCategory,
			// 'dataBrand'=>$dataBrand,
			'typeLabel'=>$typeLabel,
		)); 
	}	

	public function actionDetail($id)
	{

		$criteria=new CDbCriteria;
		$criteria->with = array('description', 'category');
		$criteria->addCondition('status = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.id = :id');
		$criteria->params[':id'] = $id;
		$data = PrdProduct::model()->find($criteria);
		if($data===null)
			throw new CHttpException(404,'The requested page does not exist.');

		if ($_GET['category'] == '') {
			if ($data->categories->category_id != null) {
				$_GET['category'] = $data->categories->category_id;
			}else{
				$_GET['category'] = 12;
			}
		}

		$criteria=new CDbCriteria;
		$criteria->addCondition('t.product_id = :product_id');
		$criteria->params[':product_id'] = $data->id;
		$criteria->order = 'id ASC';
		$attributes = PrdProductAttributes::model()->findAll($criteria);

		// $criteria=new CDbCriteria;
		// $criteria->with = array('description', 'category', 'categories');
		// $criteria->order = 'RAND()';
		// $criteria->addCondition('status = "1"');
		// $criteria->addCondition('description.language_id = :language_id');
		// $criteria->params[':language_id'] = $this->languageID;
		// $product = new CActiveDataProvider('PrdProduct', array(
		// 	'criteria'=>$criteria,
		//     'pagination'=>array(
		//         'pageSize'=>4,
		//     ),
		// ));

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.id = :id');
		$criteria->params[':id'] = $_GET['category'];
		$category = PrdCategory::model()->find($criteria);

		$criteria=new CDbCriteria;
		$criteria->with = array('description', 'category', 'categories');
		$criteria->order = 'date DESC';
		$criteria->addCondition('status = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.category_id = :category');
		$criteria->params[':category'] = $data->category_id;
		$criteria->addCondition('t.id != :id');
		$criteria->params[':id'] = $data->id;
		$criteria->limit = 3;
		$product = PrdProduct::model()->findAll($criteria);

	    $session=new CHttpSession;
	    $session->open();
	    $login_member = $session['login_member'];

		$criteria = new CDbCriteria;
		$criteria->select = 'SUM(rating) as rating';
		$criteria->addCondition('product_id = :product_id');
		$criteria->params[':product_id'] = $id;
		$criteria->addCondition('status = :status');
		$criteria->params[':status'] = 1;
		// $criteria->order = 'date DESC';
		// $criteria->group = 'product_id';

		$this->pageTitle = $data->description->name.' | '.$this->pageTitle;
		$this->layout='//layouts/column2';
		// detail_list
		$this->render('product_details', array(	
			'data' => $data,
			'model' => $model,
			'product' => $product,
			'attributes' => $attributes,
			'category' => $category,
		));
	}
	
	public function actionAddcart()
	{
		if ($_POST['id'] != '') {
			if ( ! $_POST['id'])
				throw new CHttpException(404,'The requested page does not exist.');

			if ($_POST['qty'] < 1){
				Yii::app()->user->setFlash('danger','Item can not be less than 1');
				$this->redirect(array('/product/detail', 'id'=>$_POST['id']));
			}

			if ($_POST['option'] != '') {
				$id = $_POST['id'].'-'.$_POST['option'];
			}else{
				$id = $_POST['id'];
			}
			$qty = $_POST['qty'];
			$optional = $_POST['optional'];
			$option = $_POST['option'];

			$model = new Cart;

			$data = PrdProduct::model()->findByPk($id);

			if (is_null($data))
				throw new CHttpException(404,'The requested page does not exist.');

		    $session=new CHttpSession;
		    $session->open();
		    $login_member = $session['login_member'];

			$harga = $data->harga;
			if ($login_member['type'] == 'member') {
				// $harga = $data->harga - (0.1 * $data->harga);
			}


			$model->addCart($id, $qty, $harga, $option, $optional);
			
			Yii::app()->user->setFlash('addcart',$qty);
			Yii::app()->user->setFlash('success','The item has been added to the shopping cart');
			Yii::app()->user->setFlash('openpop','1');
			$this->redirect(array('/product/detail', 'id'=>$_POST['id']));
		}else{
			$criteria=new CDbCriteria;
			$criteria->with = array('description');
			$criteria->addCondition('status = "1"');
			$criteria->addCondition('description.language_id = :language_id');
			$criteria->params[':language_id'] = $this->languageID;
			$criteria->addCondition('t.id = :id');
			$criteria->params[':id'] = $_GET['id'];
			$data = PrdProduct::model()->find($criteria);
			if($data===null)
				throw new CHttpException(404,'The requested page does not exist.');
			
			$model = new Cart;
			$cart = $model->viewCart($this->languageID);

			$this->render('addcart', array(	
				'data' => $data,
				'cart' => $cart[$_GET['id']],
			));
		}
	}

	public function actionAddcart2()
	{
		if ( ! $_GET['id'])
			throw new CHttpException(404,'The requested page does not exist.');

		$id = $_GET['id'];
		$qty = 1;
		$optional = $_POST['optional'];
		$option = $_POST['option'];

		$model = new Cart;

		$data = PrdProduct::model()->findByPk($id);

		if (is_null($data))
			throw new CHttpException(404,'The requested page does not exist.');

		$model->addCart($id, $qty, $data->harga, $option, $optional);
		
		Yii::app()->user->setFlash('addcart',$qty);
		$this->redirect(array('/product/addcart', 'id'=>$data->id));
	}

	public function actionEdit()
	{
		if ( ! $_POST['id'])
			throw new CHttpException(404,'The requested page does not exist.');

		$id = $_POST['id'];
		$qty = $_POST['qty'];
		$optional = $_POST['optional'];
		$option = $_POST['option'];

		$model = new Cart;

		$data = PrdProduct::model()->findByPk($id);

		if (is_null($data))
			throw new CHttpException(404,'The requested page does not exist.');

		$model->addCart($id, $qty, $data->harga, $option, $optional, 'edit');

		// $this->redirect(CHtml::normalizeUrl(array('/cart/shop')));
	}
	
	public function actionDestroy()
	{
		$model = new Cart;
		$model->destroyCart();
	}

	public function actionAddcompare($id)
	{
		$model = new Cart;
		$model->addCompare($id);
	}
	
	public function actionDeletecompare()
	{
		$model = new Cart;
		$model->deleteCompare($id);
		$this->redirect(CHtml::normalizeUrl(array('/product/index')));
	}
	public function actionViewcompare()
	{
		$model = new Cart;
		$data = $model->viewCompare($id);

		$this->layout='//layoutsAdmin/mainKosong';

		$categoryName = Product::model()->getCategoryName();

		$this->render('viewcompare', array(
			'data'=>$data,
			'categoryName'=>$categoryName,
		));
	}

}